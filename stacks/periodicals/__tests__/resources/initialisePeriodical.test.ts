jest.mock('../../src/services/periodical', () => ({
  fetchPeriodical: jest.fn().mockReturnValue(Promise.resolve()),
}));
jest.mock('../../src/services/initialisePeriodical', () => ({
  initialisePeriodical: jest.fn().mockReturnValue(Promise.resolve()),
}));

import { initialisePeriodical } from '../../src/resources/initialisePeriodical';

const mockContext = {
  body: {},
  database: {} as any,
  session: { identifier: 'Arbitrary ID' },

  headers: {},
  method: 'POST' as 'POST',
  params: [],
  path: '/',
  query: null,
};

it('should error when the user does not have an acount', () => {
  const promise = initialisePeriodical({ ...mockContext, session: new Error('No session') });

  return expect(promise).rejects.toEqual(new Error('You do not appear to be logged in.'));
});

it('should error when the service errors and log it', (done) => {
  const mockedService = require.requireMock('../../src/services/initialisePeriodical');
  mockedService.initialisePeriodical.mockReturnValueOnce(Promise.reject('Some error'));
  console.log = jest.fn();

  const promise = initialisePeriodical(mockContext);

  expect(promise).rejects.toEqual(new Error('There was a problem creating a new journal, please try again.'));

  setImmediate(() => {
    expect(console.log.mock.calls.length).toBe(1);
    expect(console.log.mock.calls[0][0]).toBe('Database error:');
    expect(console.log.mock.calls[0][1]).toBe('Some error');
    done();
  });
});

it('should return the Periodical details after successful initialisation', () => {
  const mockedService = require.requireMock('../../src/services/initialisePeriodical');
  mockedService.initialisePeriodical.mockReturnValueOnce({ id: 'Some ID' });

  const promise = initialisePeriodical({
    ...mockContext,
    session: { identifier: 'Some creator' },
  });

  return expect(promise).resolves.toHaveProperty('result.identifier');
});
