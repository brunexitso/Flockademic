jest.mock('../../src/services/periodical', () => ({
  fetchPeriodical: jest.fn().mockReturnValue(Promise.resolve({
    creator: {
      identifier: 'Arbitrary account id',
    },
    identifier: 'arbitrary_identifier',
  })),
}));

import { fetchPeriodical } from '../../src/resources/fetchPeriodical';

const mockContext = {
  database: {} as any,
  session: { identifier: 'Arbitrary ID' },

  body: undefined,
  headers: {},
  method: 'GET' as 'GET',
  params: [ '/arbitrary_id/', 'arbitrary_id' ],
  path: '/arbitrary_id/',
  query: null,
};

it('should error when no Journal ID was specified', () => {
  const promise = fetchPeriodical({ ...mockContext, params: [], path: '/' });

  return expect(promise).rejects.toEqual(new Error('Could not find a journal without a journal ID'));
});

it('should error when the given Journal could not be found', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.fetchPeriodical.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const promise = fetchPeriodical(mockContext);

  return expect(promise).rejects.toEqual(new Error('Could not find a Journal with ID `arbitrary_id`.'));
});

it('should call the command handler when all parameters are correct', (done) => {
  const mockedCommandHandler = require.requireMock('../../src/services/periodical');

  const promise = fetchPeriodical({
    ...mockContext,
    params: [ '/some_id/', 'some_id' ],
    path: '/some_id/',
    session: { identifier: 'Arbitrary creator' },
  });

  setImmediate(() => {
    expect(mockedCommandHandler.fetchPeriodical.mock.calls.length).toBe(1);
    expect(mockedCommandHandler.fetchPeriodical.mock.calls[0][1]).toBe('some_id');
    done();
  });
});

it('should not pass an error to the fetchPeriodical service if the user\'s session could not be found', (done) => {
  const mockedCommandHandler = require.requireMock('../../src/services/periodical');

  const promise = fetchPeriodical({
    ...mockContext,
    params: [ '/some_id/', 'some_id' ],
    path: '/some_id/',
    session: new Error('No session found'),
  });

  setImmediate(() => {
    expect(mockedCommandHandler.fetchPeriodical.mock.calls.length).toBe(1);
    expect(mockedCommandHandler.fetchPeriodical.mock.calls[0][2]).toBeUndefined();
    done();
  });
});
