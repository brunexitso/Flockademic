jest.mock('jsonwebtoken', () => ({
  sign: jest.fn().mockImplementation(() => 'arbitrary token'),
}));
process.env.jwt_secret = 'mock_secret';

import { signJwt } from '../../lambda/signJwt';

it('should respond with an error if no JWT secret was set during deployment', () => {
  const oldJwtSecret = process.env.jwt_secret;
  delete process.env.jwt_secret;
  console.log = jest.fn();

  const response = signJwt({ id: 'arbitrary account' });

  expect(console.log.mock.calls.length).toBe(1);
  expect(console.log.mock.calls[0][0])
    .toBe('Deployment error: no JSON Web Token secret was defined in $TF_VAR_jwt_secret');
  expect(response).toEqual(new Error('We are currently experiencing issues, please try again later.'));

  process.env.jwt_secret = oldJwtSecret;
});

it('should set an expiry time of no more than twenty minutes to prevent abuse when tokens are stolen', () => {
  const mockedJsonwebtoken = require.requireMock('jsonwebtoken');

  signJwt({ id: 'arbitrary account' });

  expect(mockedJsonwebtoken.sign.mock.calls.length).toBe(1);
  expect(mockedJsonwebtoken.sign.mock.calls[0][2].expiresIn).toBeDefined();
  expect(mockedJsonwebtoken.sign.mock.calls[0][2].expiresIn).toBeLessThanOrEqual(20 * 60);
});

it('should sign the JWT with an algorithm', () => {
  const mockedJsonwebtoken = require.requireMock('jsonwebtoken');

  signJwt({ id: 'arbitrary account' });

  expect(mockedJsonwebtoken.sign.mock.calls.length).toBe(1);
  expect(mockedJsonwebtoken.sign.mock.calls[0][2].algorithm).toBeDefined();
  expect(mockedJsonwebtoken.sign.mock.calls[0][2].algorithm).not.toBe('none');
});
